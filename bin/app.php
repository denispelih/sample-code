#!/usr/bin/env php
<?php

require __DIR__.'/../vendor/autoload.php';


use Infoskidka\Command\XmlGenerateCommand;
use Infoskidka\Command\XmlToHtmlProcessingCommand;
use Symfony\Component\Console\Application;

$application = new Application();
$application->add(new XmlGenerateCommand());
$application->add(new XmlToHtmlProcessingCommand());
$application->run();