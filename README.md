# XSLT Processor

### Install
```sh
$ composer install
```

### Help
```sh
$ php bin/app.php help
```

### Sample
```sh
$ php bin/app.php infoskidka:xml:generate main 3 3 5
$ php bin/app.php infoskidka:xml-to-html main 3
```
See files:
* src/Resources/xml/main.xml
* src/Resources/html/main.html


## Задание
 
**Создать функцию, которая будет генерировать случайное DOM-дерево по следующим параметрам:**
* глубина заполнения (максимальная глубина залегания листьев дерева)
* максимальное количество подчинённых узлов у каждого узла дерева ($maxNodes)
* минимальное количество подчинённых узлов у каждого узла дерева ($minNodes)
	
У каждого узла количество подчинённых должно быть rand ($minNodes, $maxNodes)

Имена узлов заполнить случайными строками из символов [a-zA-Z0-9], длина строки: 3-15 символов.
  
Дерево должно выглядеть примерно так: http://test.infoskidka.ru/php_test/data.xml
После генерации дерева его нужно сохранить в XML-файл.	
	
**Создать XSLT-преобразование, которое по дереву из п. 1 будет формировать HTML и сохранять его в валидный HTML-файл (т. е. чтоб в браузере результат был виден визуально в виде дерева)**.

Имена листьев (концевых узлов) должны выводиться в таблице по N штук в строке. 

Число N должно быть параметром внутри шаблона XSLT-преобразования и нужно иметь возможность легко его изменять.

Результат преобразования должен выглядеть таким образом (N=3): 
http://test.infoskidka.ru/php_test/data.html 

