<?php


namespace Infoskidka;


class NodeNameGenerator
{
    const CHARACTERS = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const MIN_LENGTH = 3;
    const MAX_LENGTH = 15;


    public function generateName()
    {
        $name             = '';
        $length           = $this->getLength();
        $characters       = static::CHARACTERS;
        $charactersLength = strlen(static::CHARACTERS);

        for ($i = 0; $i <= $length; $i++) {
            $name .= $characters[rand(0, $charactersLength-1)];
        }

        return $name;
    }


    private function getLength()
    {
        return rand(static::MIN_LENGTH, static::MAX_LENGTH);
    }
} 