<?php


namespace Infoskidka;


class XmlToHtmlProcessor
{
    const XSL_FILE_NAME = 'main';


    /** @var  int */
    private $columnCount;

    /** @var  string */
    private $html;

    /** @var  FileManager */
    private $fileManager;


    public function __construct(FileManager $fileManager)
    {
        $this->fileManager = $fileManager;
    }


    public function setColumnCount($columnCount)
    {
        $this->columnCount = $columnCount;
    }


    public function generateHtml($fileName)
    {
        $xml = $this->createXmlDocument($fileName);
        $xsl = $this->createXslDocument();

        $processor  = $this->createXsltProcessor($xsl);
        $this->html = $processor->transformToXml($xml);

        return $this->html;
    }


    public function createXmlDocument($fileName)
    {
        $document = new \DOMDocument();
        $document->load($this->fileManager->getPath($fileName, 'xml'));

        return $document;
    }


    public function createXslDocument()
    {
        $fileName = static::XSL_FILE_NAME;

        $document = new \DOMDocument();
        $document->load($this->fileManager->getPath($fileName, 'xsl'));

        return $document;
    }


    private function createXsltProcessor(\DOMDocument $xsl)
    {
        $processor = new \XSLTProcessor();
        $processor->importStylesheet($xsl);
        $processor->setParameter('', 'columnCount', $this->columnCount);

        return $processor;
    }
} 