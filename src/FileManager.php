<?php


namespace Infoskidka;


class FileManager
{
    private $basePath;


    public function __construct()
    {
        $this->basePath = __DIR__ . '/Resources';
    }


    public function saveXml($fileName, $content)
    {
        $this->saveFile($fileName, 'xml', $content);
    }


    public function saveHtml($fileName, $content)
    {
        $this->saveFile($fileName, 'html', $content);
    }


    public function getPath($fileName, $type)
    {
        return "$this->basePath/$type/$fileName.$type";
    }


    private function saveFile($fileName, $type, $content)
    {
        $file = $this->getPath($fileName, $type);

        $this->createDirIfNotExists($file);
        file_put_contents($file, $content);
    }


    private function createDirIfNotExists($file)
    {
        if (is_dir($dir = dirname($file))) {
            return;
        }
        mkdir($dir);
    }
} 