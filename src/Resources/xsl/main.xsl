<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru">
            <head>
                <style type="text/css">
                    div.treeNode { padding-left: 20px; display: block; }
                    table tr     { display: block; }
                    table td 	 { border: 1px solid black; }
                </style>
            </head>

            <body>
                <xsl:apply-templates select="treeNode"/>
            </body>
        </html>

    </xsl:template>


    <xsl:template match="//treeNode[count(.//treeNode) > 0]">
        <div class="treeNode">
            <xsl:value-of select="@name"/>
            <xsl:apply-templates select="treeNode"/>
        </div>
    </xsl:template>


    <xsl:template match="treeNode" mode="td">
        <td>
            <xsl:value-of select="@name"/>
        </td>
    </xsl:template>


    <xsl:template match="//treeNode[count(./treeNode) > 0 and count(./treeNode/treeNode) = 0]">
        <div class="treeNode">
            <xsl:value-of select="@name"/>

            <table>
                <xsl:variable name="c">
                    <xsl:value-of select="count(./treeNode)"/>
                </xsl:variable>

                <xsl:for-each select="./treeNode">


                    <xsl:choose>

                        <xsl:when test="(position() mod $columnCount = 0)">
                            <xsl:variable name="pos">
                                <xsl:value-of select="position()"/>
                            </xsl:variable>

                            <tr>

                                <xsl:for-each select="../treeNode[($pos >= position()) and (position() > ($pos - $columnCount))]">
                                    <xsl:apply-templates select="." mode="td"/>
                                </xsl:for-each>

                            </tr>
                        </xsl:when>


                        <xsl:when test="($c div $columnCount = position() div $columnCount and $c > $columnCount)">
                            <xsl:variable name="pos">
                                <xsl:value-of select="position()"/>
                            </xsl:variable>

                            <tr>

                                <xsl:for-each select="../treeNode[($pos >= position()) and (position() > ($pos - $c mod $columnCount))]">
                                    <xsl:apply-templates select="." mode="td"/>
                                </xsl:for-each>

                            </tr>
                        </xsl:when>


                        <xsl:when test="($c div $columnCount = position() div $columnCount and $columnCount > $c)">
                            <tr>
                                <xsl:for-each select="../treeNode">
                                    <xsl:apply-templates select="." mode="td"/>
                                </xsl:for-each>
                            </tr>
                        </xsl:when>

                    </xsl:choose>

                </xsl:for-each>
            </table>

        </div>
    </xsl:template>


</xsl:stylesheet>