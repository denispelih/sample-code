<?php

namespace Infoskidka\Command;

use Infoskidka\FileManager;
use Infoskidka\NodeNameGenerator;
use Infoskidka\XmlGenerator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class XmlGenerateCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('infoskidka:xml:generate')
            ->setDescription('Generate xml file')
            ->addArgument(
                'file',
                InputArgument::REQUIRED,
                'File name for saving xml'
            )
            ->addArgument(
                'deep',
                InputArgument::REQUIRED,
                'Max degries count'
            )
            ->addArgument(
                'min',
                InputArgument::REQUIRED,
                'Min children count'
            )
            ->addArgument(
                'max',
                InputArgument::REQUIRED,
                'Max children count'
            );
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $fileName     = $input->getArgument('file');
        $deep         = $input->getArgument('deep');
        $min          = $input->getArgument('min');
        $max          = $input->getArgument('max');

        $nodeNameGenerator = new NodeNameGenerator();
        $xmlGenerator      = new XmlGenerator($nodeNameGenerator);
        $fileManager       = new FileManager();

        $xmlGenerator->setDeep($deep);
        $xmlGenerator->setMinChildrenCount($min);
        $xmlGenerator->setMaxChildrenCount($max);

        $xml = $xmlGenerator->generate();
        $fileManager->saveXml($fileName, $xml);
    }
} 