<?php


namespace Infoskidka\Command;


use Infoskidka\FileManager;
use Infoskidka\XmlToHtmlProcessor;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class XmlToHtmlProcessingCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('infoskidka:xml-to-html')
            ->setDescription('Generate html file')
            ->addArgument(
                'file',
                InputArgument::REQUIRED,
                'File name saved xml'
            )
            ->addArgument(
                'column-count',
                InputArgument::REQUIRED,
                'Columns count in html table'
            );
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $fileName    = $input->getArgument('file');
        $columnCount = $input->getArgument('column-count');
        $fileManager = new FileManager();

        $processor   = new XmlToHtmlProcessor($fileManager);
        $processor->setColumnCount($columnCount);

        $html = $processor->generateHtml($fileName);
        $fileManager->saveHtml($fileName, $html);
    }
} 