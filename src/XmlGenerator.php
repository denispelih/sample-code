<?php

namespace Infoskidka;

class XmlGenerator
{
    const ELEMENT_NAME   = 'treeNode';


    /** @var  int */
    private $deep   = 0;
    /** @var  int */
    private $degree = 0;

    /** @var  int */
    private $minChildrenCount = 0;
    /** @var  int */
    private $maxChildrenCount = 0;

    /** @var  \DOMDocument */
    private $document;
    /** @var  NodeNameGenerator */
    private $nodeNameGenerator;


    public function __construct(NodeNameGenerator $nodeNameGenerator)
    {
        $this->document          = new \DOMDocument();
        $this->nodeNameGenerator = $nodeNameGenerator;
    }


    public function setDeep($deep)
    {
        $this->deep = $deep;
    }


    public function setMaxChildrenCount($maxChildrenCount)
    {
        $this->maxChildrenCount = $maxChildrenCount;
    }


    public function setMinChildrenCount($minChildrenCount)
    {
        $this->minChildrenCount = $minChildrenCount;
    }


    public function generate()
    {
        $rootNode = $this->createNode();
        $this->addChildrenTo($rootNode);

        $this->document->appendChild($rootNode);

        return $this->document->saveXML();
    }


    private function addChildrenTo(\DOMNode $node)
    {
        $childrenCount = $this->getChildrenCount();

        for ($i = 0; $i < $childrenCount; $i++) {
            $child = $this->createNode();

            if (++$this->degree < $this->deep) {
                $this->addChildrenTo($child);
            }
            --$this->degree;

            $node->appendChild($child);
        }
    }


    private function createNode()
    {
        $child = $this->document->createElement(static::ELEMENT_NAME);
        $this->addNameTo($child);

        return $child;
    }


    private function addNameTo(\DOMNode $node)
    {
        $nameAttribute        = $this->document->createAttribute('name');
        $nameAttribute->value =  $this->nodeNameGenerator->generateName();

        $node->appendChild($nameAttribute);
    }


    private function getChildrenCount()
    {
        return rand($this->minChildrenCount, $this->maxChildrenCount);
    }


}